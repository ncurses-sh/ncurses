FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > ncurses.log'

COPY ncurses .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' ncurses
RUN bash ./docker.sh

RUN rm --force --recursive ncurses
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD ncurses
